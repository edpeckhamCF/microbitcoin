#include "spi.h"

		SPI_HandleTypeDef*	SPI;
		bool				SPI_RxITRequired;
		uint8_t*			SPI_RxBufferPtr;

static 	uint8_t				SPI_RxBuffer[SPI_RXBUFFER_SIZE];
static 	uint16_t			SPI_RxBufferLen;
static  void(*SPI_RxCallback)(uint8_t*,uint8_t**,uint16_t*);

bool SPI_Init(SPI_HandleTypeDef* spi, void(*rxcallback)(uint8_t*,uint8_t**,uint16_t*))
{
	SPI = spi;
	SPI_RxITRequired = FALSE;
	SPI_RxBufferLen = 0;
	SPI_RxBufferPtr = SPI_RxBuffer;
	SPI_RxCallback = rxcallback;

	HAL_SPI_Receive_IT(spi, SPI_RxBufferPtr, 1);
	return TRUE;
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef* spi)
{
	if (spi->RxXferSize != 1) {
		return;
	}

	SPI_RxBufferPtr++;
	SPI_RxBufferLen++;
	if (SPI_RxBufferPtr == SPI_RxBuffer+SPI_RXBUFFER_SIZE) {
		SPI_RxBufferPtr = SPI_RxBuffer;
		SPI_RxBufferLen = 0;
	}

	if (SPI_RxCallback) {
		SPI_RxCallback(SPI_RxBuffer, &SPI_RxBufferPtr, &SPI_RxBufferLen);
	}

	SPI_RxITRequired = TRUE;
}

bool SPI_Transmit(uint8_t* data, uint16_t size)
{
	return HAL_SPI_Transmit_DMA(SPI, data, size) == HAL_OK;
}
