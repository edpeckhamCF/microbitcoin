#include "i2c.h"

		I2C_HandleTypeDef* 	I2C;
		bool				I2C_RxITRequired;
		uint8_t				I2C_Address;
		uint8_t*			I2C_RxBufferPtr;

static 	uint8_t				I2C_RxBuffer[I2C_RXBUFFER_SIZE];
static 	uint16_t			I2C_RxBufferLen;
static  void(*I2C_RxCallback)(uint8_t*,uint8_t**,uint16_t*);

bool I2C_Init(I2C_HandleTypeDef* i2c, uint8_t address, void(*rxcallback)(uint8_t*,uint8_t**,uint16_t*))
{
	I2C = i2c;
	I2C_Address = address;
	I2C_RxITRequired = FALSE;
	I2C_RxBufferLen = 0;
	I2C_RxBufferPtr = I2C_RxBuffer;
	I2C_RxCallback = rxcallback;

	HAL_I2C_Master_Receive_IT(i2c, I2C_Address, I2C_RxBufferPtr, 1);

	return TRUE;
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef* i2c)
{
	if (i2c->XferSize != 1) {
		return;
	}

	I2C_RxBufferPtr++;
	I2C_RxBufferLen++;
	if (I2C_RxBufferPtr == I2C_RxBuffer+I2C_RXBUFFER_SIZE) {
		I2C_RxBufferPtr = I2C_RxBuffer;
		I2C_RxBufferLen = 0;
	}

	if (I2C_RxCallback) {
		I2C_RxCallback(I2C_RxBuffer, &I2C_RxBufferPtr, &I2C_RxBufferLen);
	}

	I2C_RxITRequired = TRUE;
}

bool I2C_Transmit(uint8_t* data, uint16_t size)
{
	return HAL_I2C_Master_Transmit_DMA(I2C, I2C_Address, data, size) == HAL_OK;
}
