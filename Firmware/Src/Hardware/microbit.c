#include "microbit.h"
#include "spi.h"

static void MICROBIT_RxCallback(uint8_t* buffer, uint8_t** bufferptr, uint16_t* bufferlen);

bool MICROBIT_Init(SPI_HandleTypeDef* spi)
{
	return SPI_Init(spi, MICROBIT_RxCallback);
}

static void MICROBIT_RxCallback(uint8_t* buffer, uint8_t** bufferptr, uint16_t* bufferlen)
{

}
