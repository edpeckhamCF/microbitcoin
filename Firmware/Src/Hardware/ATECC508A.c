#include "ATECC508A.h"
#include "i2c.h"

static void ATECC508A_RxCallback(uint8_t* buffer, uint8_t** bufferptr, uint16_t* bufferlen);

bool ATECC508A_Init(I2C_HandleTypeDef* i2c)
{
    return I2C_Init(i2c, ATECC508A_I2C_ADDRESS, ATECC508A_RxCallback);
}

static void ATECC508A_RxCallback(uint8_t* buffer, uint8_t** bufferptr, uint16_t* bufferlen)
{

}
