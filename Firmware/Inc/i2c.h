#ifndef I2C_H
#define I2C_H

#include "general.h"

#define I2C_RXBUFFER_SIZE		128

I2C_HandleTypeDef* 	I2C;
bool				I2C_RxITRequired;
uint8_t				I2C_Address;
uint8_t*			I2C_RxBufferPtr;

bool I2C_Init(I2C_HandleTypeDef* i2c, uint8_t address, void(*rxcallback)(uint8_t*,uint8_t**,uint16_t*));
bool I2C_Transmit(uint8_t* data, uint16_t size);

#endif
