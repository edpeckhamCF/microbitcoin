#ifndef ATECC508A_H
#define ATECC508A_H

#include "general.h"

#define ATECC508A_I2C_ADDRESS		0x80

bool ATECC508A_Init(I2C_HandleTypeDef* i2c);

#endif
