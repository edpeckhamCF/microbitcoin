#ifndef GENERAL_H
#define GENERAL_H
    // Includes
    #include "stm32f0xx_hal.h"
    #include <stdlib.h>
    #include <stdbool.h>

    // Defines
    #define    DEBUG                                1
    #define    TRUE                                1
    #define FALSE                                0
    #define PACKED                                 __attribute__((packed))
    #define ALIGN(x)                            __attribute__((aligned(x)))
    #define TIDY(o)                                if (o) { MEM_FREE(o); o = 0; }

#if (DEBUG == 2)
    #define DEBUG_PRINT(str)                    PRINT(str)
    #define MEM_FREE(p)                            FREE(p)
    #define MEM_MALLOC(s)                        MALLOC(s)
    #define MEM_CALLOC(l,s)                        CALLOC(l,s)
    #define MEM_REALLOC(p,s)                    REALLOC(p,s)
#else
    #define    DEBUG_PRINT(str)
    #define MEM_FREE(p)                            free(p)
    #define MEM_MALLOC(s)                        malloc(s)
    #define MEM_CALLOC(l,s)                        calloc(l,s)
    #define MEM_REALLOC(p,s)                    realloc(p,s)
#endif

#endif
