#ifndef SPI_H
#define SPI_H

#include "general.h"

#define SPI_RXBUFFER_SIZE		128

SPI_HandleTypeDef*	SPI;
bool				SPI_RxITRequired;
uint8_t*			SPI_RxBufferPtr;

bool SPI_Init(SPI_HandleTypeDef* spi, void(*rxcallback)(uint8_t*,uint8_t**,uint16_t*));
bool SPI_Transmit(uint8_t* data, uint16_t size);

#endif
